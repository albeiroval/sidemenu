 <?php

/*------------------------------------------------------------------------------------------------*/

include( 'config.php' );
include( TWEB_PATH . 'core.php' );
include( 'core.sidebar.php' );

/*------------------------------------------------------------------------------------------------*/

$oWeb = new TWeb( 'Prueba de TWeb con Sidebar - 1.0a' );
$oWeb->lAwesome = true; 
$oWeb->lPreloader = true;

$oWeb->SetIcon( 'images/myicon.png' );
$oWeb->SetBrush( 'images/myback1.png' );

$oWeb->AddJs( 'myfunctions.js' );
$oWeb->AddCss( 'mystyles.css' );

$oWeb->Activate();

$oSidebar = new TSidebar( "mysidebar", "MENU PRINCIPAL", 200, 'images/employee.png' );
$oSidebar->debug=true;
$oSidebar->Title( 'SISTEMA PRUEBA', '#6C3483', '16px' );
$oSidebar->ItemsColors( 'black', 'white' );
$oSidebar->ItemsHoverColors( 'white', '#2980B9', '#6C3483' );

$o = $oSidebar->AddOption( "sbo01", "Home"     , "Click1()" );
$o = $oSidebar->AddOption( "sbo02", "Services" , "Click2()" );
$o = $oSidebar->AddOption( "sbo03", "Customers", "Click3()" );
$o = $oSidebar->AddOption( "sbo04", "About"    , "Click4()" );
$o = $oSidebar->AddOption( "sbo05", "Contact"  , "Click5()" );
$o = $oSidebar->AddOption( "sbo06", "Home"     , "Click6()" );

$oSidebar->Activate();

$oWeb->End();

?>

<script type="text/javascript">

function Click1(){
	MsgNotify( 'Click: 1', 'info', true );
	toggleSidebar();
}

function Click2(){
	MsgNotify( 'Click: 2', 'info', true );
	toggleSidebar();
}

function Click3(){
	MsgNotify( 'Click: 3', 'info', true );
	toggleSidebar();
}

function Click4(){
	MsgNotify( 'Click: 4', 'info', true );
	toggleSidebar();
}

function Click5(){
	MsgNotify( 'Click: 5', 'info', true );
	toggleSidebar();
}

</script>

<?php


?>