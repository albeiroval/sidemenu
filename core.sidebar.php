<?php

/*---------------------------------------------------------------------------------------------*/

class TSidebar
{
	private $cId           = '';
	private $Options       = array();
	public  $Title         = '';
	public  $TitleColor    = 'yellow';
	public  $TitleSize     = '16px';
	public  $ItemColor     = 'rgba(230,230,230,0.9)';
	public  $ItemBack      = '#151719';
	public  $LeftItemColor = '#f1c40f';
	public  $ItemOverColor = 'cyan';
	public  $ItemOverBack  = 'navy';
	private $Width         = 0;
	private $Image         = null;
	public  $debug         = false;

	/**/

	public function __construct( $cId = '', $Title = 'MAIN MENU', $Width = 200, $Image = null )
	{
		$this->cId       = $cId;
		$this->Title     = $Title;
		$this->Width     = $Width;
		$this->Image     = $Image;
	}

	/**/

	public function Title( $Title = 'MAIN MENU', $Color = 'yellow', $Size = '16px' )
	{
		$this->Title      = $Title;
		$this->TitleColor = $Color;
		$this->TitleSize  = $Size;
	}

	/**/

	public function ItemsColors( $Color = 'cyan', $Background = 'navy' ) {
		$this->ItemColor = $Color;
		$this->ItemBack  = $Background;
	}

	/**/

	public function ItemsHoverColors( $Color = 'cyan', $Background = 'navy', $LeftColorBorder = '#f1c40f' ) {
		$this->ItemOverColor = $Color;
		$this->ItemOverBack  = $Background;
		$this->LeftItemColor = $LeftColorBorder;
	}

	/**/

	public function AddOption( $cId, $cOption, $cFunction )
	{
		$oOption = new TSidebarOption( $cId, $cOption, $cFunction );
		$this->Options[] = $oOption;

		return $oOption;
	}

	/**/

	public function Activate()
	{
		$Width = $this->Width . 'px';
		$PosToggleBtn = ( $this->Width + 10 ) . 'px';

		$cHtml  = '<div id="sidebar">' . PHP_EOL;

		if( ! empty( $this->Image ) )
		{
			$cHtml .= ' <div class="sbimage">' . PHP_EOL;
			$cHtml .= '   <img src="' . $this->Image . '" alt="sidebar_image" style="width: 80px; border-radius: 50%">' . PHP_EOL;
			$cHtml .= ' </div>'                . PHP_EOL;
		}

		$cHtml .= '<div class="sbtitle">' . $this->Title . '</div>' . PHP_EOL;

		$cHtml .= ' <div class="toogle-btn" onclick="toggleSidebar()">' . PHP_EOL;
		//$cHtml .= '  <span>&#9776;</span>'                              . PHP_EOL;
		$cHtml .= '    <i id="test1" class="fa fa-bars" style="font-size:30px;"></i>' . PHP_EOL;
		$cHtml .= ' </div>'                                             . PHP_EOL;

		$cHtml .= ' <ul>' . PHP_EOL;
		$nCount = count( $this->Options );
		for ( $i = 0; $i < $nCount - 1; $i++ )
		{
			$cId       = $this->Options[ $i ]->cId;
			$cOption   = $this->Options[ $i ]->cOption;
			$cFunction = $this->Options[ $i ]->bOnClick;

			$cHtml .= '  <li ' . 'id="' . $cId . '" onclick="' . $cFunction . '">' . $cOption . '</li>' . PHP_EOL;
		}
		$cHtml .= ' </ul>' . PHP_EOL;

		$cHtml .= '</div>' . PHP_EOL;

		$cHtml .= PHP_EOL;

		$cHtml .= '<style>' . PHP_EOL;

		$cHtml .= '#sidebar {'                              . PHP_EOL;
		$cHtml .= ' font-family: sans-serif;'               . PHP_EOL;
		$cHtml .= ' position : fixed;'                      . PHP_EOL;
		$cHtml .= ' width : ' . $Width . ';'                . PHP_EOL;
		$cHtml .= ' height : 100%;'                         . PHP_EOL;
		$cHtml .= ' left : -' . $Width. ';'                 . PHP_EOL;
		$cHtml .= ' background : ' . $this->ItemBack  . ';' . PHP_EOL;
		$cHtml .= ' box-shadow : 5px 0 5px -2px #888;'      . PHP_EOL;
		$cHtml .= ' transition : all 500ms linear;'         . PHP_EOL;
		$cHtml .= '}'                                       . PHP_EOL;

		$cHtml .= '#sidebar.active {'  . PHP_EOL;
		$cHtml .= ' left: 0px;'        . PHP_EOL;
		$cHtml .= '}'                  . PHP_EOL;

		$cHtml .= '#sidebar .sbtitle {'                   . PHP_EOL;
		$cHtml .= ' color: ' . $this->TitleColor . ';'    . PHP_EOL;
		$cHtml .= ' font-size: ' . $this->TitleSize . ';' . PHP_EOL;
		$cHtml .= ' font-weight: bold;'                   . PHP_EOL;
		$cHtml .= ' padding: 10px 20px;'                  . PHP_EOL;
		$cHtml .= '}'                                     . PHP_EOL;

		$cHtml .= '#sidebar .sbimage {'    . PHP_EOL;
		$cHtml .= ' display: block;'       . PHP_EOL;
		$cHtml .= ' padding-top: 10px;'    . PHP_EOL;
		$cHtml .= ' padding-bottom: 10px;' . PHP_EOL;
		$cHtml .= ' text-align: center;'   . PHP_EOL;
		$cHtml .= '}'                      . PHP_EOL;

		$cHtml .= '#sidebar .toogle-btn {'        . PHP_EOL;
		$cHtml .= ' position: absolute;'          . PHP_EOL;
		$cHtml .= ' left: ' . $PosToggleBtn . ';' . PHP_EOL;
		$cHtml .= ' top: 10px;'                   . PHP_EOL;
		$cHtml .= ' cursor: pointer;'             . PHP_EOL;
		$cHtml .= '}'                             . PHP_EOL;

		$cHtml .= '#sidebar .toogle-btn span {' . PHP_EOL;
		$cHtml .= ' display: block;'            . PHP_EOL;
		//$cHtml .= ' width: 40px;'               . PHP_EOL;
		//$cHtml .= ' text-align: center;'        . PHP_EOL;
		//$cHtml .= ' font-size: 30px;'           . PHP_EOL;
		//$cHtml .= ' font-weight: bold;'         . PHP_EOL;
		//$cHtml .= ' border: 2px solid #000;'    . PHP_EOL;
		$cHtml .= '}'                           . PHP_EOL;

		$cHtml .= '#sidebar ul li {'                                 . PHP_EOL;
		$cHtml .= ' color: ' . $this->ItemColor . ';'                . PHP_EOL;
		$cHtml .= ' list-style: none;'                               . PHP_EOL;
		$cHtml .= ' padding: 15px 10px;'                             . PHP_EOL;
		$cHtml .= ' border-bottom: 1px solid rgba(100,100,100,0.3);' . PHP_EOL;
		$cHtml .= ' cursor: pointer;'                                . PHP_EOL;
		$cHtml .= '}'                                                . PHP_EOL;

		$cHtml .= '#sidebar ul li:hover {'                                . PHP_EOL;
		$cHtml .= ' color: ' . $this->ItemOverColor . ';'                 . PHP_EOL;
		$cHtml .= ' background-color: ' . $this->ItemOverBack . ';'       . PHP_EOL;
		$cHtml .= ' border-left: 4px solid ' . $this->LeftItemColor . ';' . PHP_EOL;
		$cHtml .= ' padding-left: 6px;'                                   . PHP_EOL;
		$cHtml .= '}'                                                     . PHP_EOL;

		$cHtml .= '.fa-bars {'              . PHP_EOL;
		$cHtml .= ' width: 30px;'           . PHP_EOL;
		$cHtml .= ' text-align: center;'    . PHP_EOL;
		$cHtml .= ' border-width: 1px;'     . PHP_EOL;
		$cHtml .= ' border-style: solid;'   . PHP_EOL;
		$cHtml .= ' border-color: black;'   . PHP_EOL;
		$cHtml .= ' border-image: initial;' . PHP_EOL;
		$cHtml .= ' border-radius: 4px; '   . PHP_EOL;
		$cHtml .= ' padding: 6% 9% 6% 9%; ' . PHP_EOL;
		$cHtml .= '}'                       . PHP_EOL;
		
		$cHtml .= '.fa-times {'             . PHP_EOL;
		$cHtml .= ' width: 30px;'           . PHP_EOL;
		$cHtml .= ' text-align: center;'    . PHP_EOL;
		$cHtml .= ' border-width: 1px;'     . PHP_EOL;
		$cHtml .= ' border-style: solid;'   . PHP_EOL;
		$cHtml .= ' border-color: black;'   . PHP_EOL;
		$cHtml .= ' border-image: initial;' . PHP_EOL;
		$cHtml .= ' border-radius: 4px; '   . PHP_EOL;
		$cHtml .= ' padding: 6% 9% 6% 9%; ' . PHP_EOL;
		$cHtml .= '}'                       . PHP_EOL;

		$cHtml .= '</style>' . PHP_EOL;
		
		$cHtml .= PHP_EOL;

		$cHtml .= '<script>'                                                        . PHP_EOL;
		$cHtml .= 'function toggleSidebar(){'                                       . PHP_EOL;
		$cHtml .= ' var i = document.getElementById( "test1" );'                    . PHP_EOL;
		$cHtml .= ' if( i.className == "fa fa-bars" )'                              . PHP_EOL;
		$cHtml .= ' {'                                                              . PHP_EOL;
		$cHtml .= ' document.getElementById("test1").className = "fa fa-times";'    . PHP_EOL;
		$cHtml .= ' }'                                                              . PHP_EOL;
		$cHtml .= ' else'                                                           . PHP_EOL;
		$cHtml .= ' {'                                                              . PHP_EOL;
		$cHtml .= ' document.getElementById("test1").className = "fa fa-bars";'     . PHP_EOL;
		$cHtml .= ' }'                                                              . PHP_EOL;
		$cHtml .= ' document.getElementById("sidebar").classList.toggle("active");' . PHP_EOL;
		$cHtml .= '}'                                                               . PHP_EOL;
		$cHtml .= '</script>'                                                       . PHP_EOL;
		
		if( $this->debug ){
			console_php( $cHtml );
		}

		echo $cHtml;
	}
}

/*---------------------------------------------------------------------------------------------*/

Class TSidebarOption
{
	public $cId     = '';
	public $cOption = '';
	public $cFunction = '';

	/**/

	public function __construct( $cId, $cOption, $cFunction )
	{
		$this->cId   = $cId;
		$this->cOption = $cOption;
		$this->bOnClick = $cFunction;
	}

} /*End class TDataTableCol*/

/*---------------------------------------------------------------------------------------------*/

function console_php( $data )
{
	ob_start();
	$output  = "<script>console.log('PHP debugger:')</script>";
	$output .= "<script>console.log( '";
	$output .= json_encode( print_r( $data, true ) );
	$output .= "' );</script>";
	echo $output;
}

/*---------------------------------------------------------------------------------------------*/
//EOF
/*---------------------------------------------------------------------------------------------*/
